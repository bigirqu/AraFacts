# AraFacts 
This repository contains AraFacts Dataset, an Arabic dataset of naturally-occurring claims collected from 5 Arabic fact-checking websites. ([Misbar](http://misbar.com/), [Verify-sy](https://www.verify-sy.com/), [Fatabyyano](http://fatabyyano.net/), [FactuelAFP](http://factuel.afp.com/ar/) and [Maharat-news](https://maharat-news.com/fact-o-meter)). The dataset covers over 18,500 claims since 2016 and is annotated by professional fact-checkers.  

## Paper 
[AraFacts: The First Large Arabic Dataset of Naturally-Occurring Professionally-Verified Claims](https://www.aclweb.org/anthology/2021.wanlp-1.26/)


## Dataset Format
### AraFacts.csv: csv file with the following columns:
1.	**ClaimID**: ID of the claim  
2.	**claim**: Text of the claim   
3.	**description**: Detailed description of the claim   
4.	**source**: Name of the fact-checking website from which the claim was crawled   
5.	**date**: Fact-checking article publication date   
6.	**source_label**: The veracity label of the claim as it appears in the fact-checking website   
7.	**normalized_label**: Normalized claim label   
8.	**source_category**: Topical category of the claim as it appears in the fact-checking website   
9.	**normalized_category**: Normalized topical category of the claim   
10.	**source_url**: URL of the article   
11.	**claim_urls**: URLs to webpages spreading the claim   
12.	**evidence_urls**: URLs referenced by the fact-checker to justify their annotation 
13. **claim_type**: Indicating whether the claim refers to text **(0)**, an image **(1)** or a video **(2)** 
### AraFacts_content.csv: csv file with the following columns:
1. **ClaimID**: ID of the claim
2. **content**: Content of the fact-checking article for that claim


## Dataset updates 
| Latest update | Data count |
| ------ | ------ |
| 23/06/2024 |  18,598 |
| 01/03/2021 | 6,222 |
| 22/02/2021 | 6,121 |

## Contact information 
Please contact the authors of the paper for any queries: 
- [Zien Sheikh Ali ](mailto:zs1407404@qu.edu.qa)
- [Watheq Mansour](mailto:wm1900793@qu.edu.qa)
- [Tamer Elsayed](mailto:telsayed@qu.edu.qa)
- [Abdulaziz Al-Ali](mailto:a.alali@qu.edu.qa)

## Citation 
If you use AraFacts dataset, please cite our paper 

```
@inproceedings{arafacts2021,
    title={AraFacts: The First Large Arabic Dataset of Naturally-OccurringProfessionally-Verified Claims},
    author={Sheikh Ali, Zien and Mansour, Watheq and Elsayed, Tamer and Al-Ali, Abdulaziz},
    booktitle={Proceedings of the Sixth Arabic Natural Language Processing Workshop},
    pages={},
    publisher = {Association for Computational Linguistics},
    year={2021}}
```
