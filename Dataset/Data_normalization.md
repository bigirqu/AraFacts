To achieve a standard rating for all claims in **AraFacts**, we propose a claim rating scheme consisting of 4 labels to map original labels to a standard scale. We similary propose a category rating scheme consisting of 8 categories to map the original topical categories.
The tables below show the normalized ratings and their original values.  


## Claim normalization  
| Normalized Claim Label | Claim Labels in Fact-Checking Webistes |
| ------ | ------ |
| False | زائف, خرافة, غير صحيح, خطأ, احتيال, مركب, عنوان مضلل |
| Partly-false | زائف جزئيًّا, مضلل, مشكوك فيه, اثارة, صحيح جزئيًّا, غير دقيق |
| Sarcasm | ساخر |
| True  | صحيح, انتقائي, حقيقة, مجتزأ|

## Category normalization  
| Normalized Topical Category | Topical Categories in Fact-checking Webistes |
| ------ | ------ |
| Arts and culture | ثقافة وفن, موسيقى |
| Environment | بيئة, طاقة وبيئة |
| Social | اجتماعي, مأكولات و مشروبات, لايف ستايل |
| Politics | سياسي, سياسة, حوكمة, حرية التعبير, في الميدان, لجوء |
| General Sciences | علوم, تكنولوجيا |
| Health | طبي, صحة, مستجدات كورونا |
| Religion | ديني, روحانيات ودين |
| Miscellaneous | جندر, ترفيه, اخبار, سفر, أعمال, منوعات, العاب فيديو, رياضة|
